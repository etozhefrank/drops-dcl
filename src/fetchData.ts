import fetch from 'node_modules/node-fetch/@types/index'

export async function fetchOpenSea(){
    const response = await fetch('https://api.opensea.io/api/v1/asset/0xb47e3cd837ddf8e4c57f05d70ab865de6e193bbb/1/');
    const body = await response.text();

    console.log('OPENSEA: ', body);
}
