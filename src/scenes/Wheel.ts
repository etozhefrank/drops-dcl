import { getCurrentRealm } from '@decentraland/EnvironmentAPI'
import { getUserData } from '@decentraland/Identity'
import { RotatableEntity } from 'src/entities/RotatableEntity'
import resources from 'src/resources'
import utils from '../../node_modules/decentraland-ecs-utils/index'
import * as crypto from '@dcl/crypto-scene-utils'


export function Wheel(): void {
	let acceleration

	const wheel_static = new Entity()
	wheel_static.addComponent(new GLTFShape('models/drops/Wheel_static.glb'))
	wheel_static.addComponent(
		new Transform({
			position: new Vector3(-14.5, 0, 24.5),
			scale: new Vector3(150, 150, 150),
			rotation: Quaternion.Euler(0, 90, 0)
		})
	)
	engine.addEntity(wheel_static)

	// const wheel_movable = new RotatableEntity(
	// 	resources.models.wheel.wheel_moveble,
	// 	new Transform({
	// 		position: new Vector3(-14.5, 2.8, 24.5),
	// 		scale: new Vector3(150, 150, 150),
	// 		rotation: Quaternion.Euler(0, 90, 0)
	// 	}),
	// 	Quaternion.Euler(360+180, 90, 0)
	// )
	//
	// // wheel_movable.addComponent(
	// // 	new OnPointerDown((): void => {
	// // 		wheel_movable.getComponent(utils.ToggleComponent).toggle()
	// // 	})
	// // )
	//
	// wheel_movable.addComponent(
	// 	new OnPointerDown(async () => {
	// 		log('PIZDA')
	//
	// 		try {
	// 			const userData = await getUserData()
	// 			const playerRealm = await getCurrentRealm()
	//
	// 			const url = `${playerRealm.domain}/lambdas/profile/${userData.userId}`.toString()
	// 			log('using URL: ', url)
	// 			const response = await fetch(url)
	// 			const json = await response.json()
	// 			crypto.avatar.getUserInventory(userData.userId).then((inventory) => {
	// 				log('HUI: ', inventory)
	// 			})
	// 		} catch (e) {
	// 			log('errrrrorrr:', e.toString())
	// 		}
	// 		let speed: number
	//
	// 		const items = [
	// 			resources.spin.hat1,
	// 			resources.spin.hat2,
	// 			resources.spin.hat3
	// 		]
	// 		/** random acceleration + speed to exact item */
	// 		const acceleration: number = 360
	//
	// 		new Transform({
	// 			rotation: Quaternion.Euler(0, 90, 0)
	// 		}),
	//
	// 		log('SPEED: ', acceleration)
	// 		return acceleration
	// 	})
	// )

}

function fetchPlayerData() {
	throw new Error('Function not implemented.')
}
