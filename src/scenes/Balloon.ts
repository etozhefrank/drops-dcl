import { movePlayerTo } from '@decentraland/RestrictedActions'
import resources from 'src/resources'
import utils from '../../node_modules/decentraland-ecs-utils/index'

export function Balloon(): void {

	const ballonStand = new Entity()
	ballonStand.addComponent(resources.models.balloon.balloon_static)
	const ballonStandPosition = new Vector3(9, 0.2, 21)
	ballonStand.addComponent(
		new Transform({
			position: ballonStandPosition,
			scale: new Vector3(100, 100, 100),
		})
	)
	engine.addEntity(ballonStand)

	const balloon = new Entity()
	balloon.addComponent(resources.models.balloon.balloon_moveble)
	const ballonPosition = new Vector3(9, -1.4, 21)
	balloon.addComponent(
		new Transform({
			position: ballonPosition,
			scale: new Vector3(100, 100, 100),
		})
	)
	engine.addEntity(balloon)

	const path: Vector3[] = []
	path[0] = ballonPosition
	path[1] = new Vector3(0, 10, 10)
	path[2] = new Vector3(-20, 10, 10)
	path[3] = new Vector3(-15, 10, 20)
	path[4] = new Vector3(-10, 10, 30)
	path[5] = new Vector3(-0, 10, 25)
	path[6] = new Vector3(-5, 10, 20)
	// path[7] = new Vector3(-0, 10, 10)
	path[7] = ballonPosition

	const flyClick = balloon.addComponent(
		new OnPointerDown(
			() => {
				balloon.removeComponent(flyClick)
				const getBallonPosition = balloon.getComponent(Transform).position
				movePlayerTo({
					x: getBallonPosition.x,
					y: getBallonPosition.y,
					z: getBallonPosition.z,
				})
				balloon.addComponent(
					new utils.FollowPathComponent(path, 60, () => {
						log('Fly finished')
						balloon.addComponent(flyClick)
					})
				)
			},
			{ hoverText: 'Fly!' }
		)
	)
}
