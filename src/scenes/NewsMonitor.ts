import { Interval } from 'node_modules/decentraland-ecs-utils/timer/component/interval'
import resources from 'src/resources'

export function NewsMonitor(): void {
	const LINK = 'nftbastards'

	const tgStand = new Entity()
	tgStand.addComponent(resources.models.newsMonitor.telegram_stand)
	tgStand.addComponent(
		new Transform({
			scale: new Vector3(15, 15, 15),
			position: new Vector3(-13, 0.2, 5),
			rotation: Quaternion.Euler(0, -90, 0),
		})
	)

	const tgTexture = new Texture('https://puppeteer.argreality.com/screenshot/tg.png')

	const tgMaterial = new Material()
	tgMaterial.albedoTexture = tgTexture

	const planeMonitor = new Entity()
	planeMonitor.addComponent(new PlaneShape())
	planeMonitor.addComponent(tgMaterial)
	planeMonitor.addComponent(
		new Transform({
			scale: new Vector3(1.32, 2 * 1.32, 1.32),
			position: new Vector3(-13, 1.7, 5.001),
			rotation: Quaternion.Euler(-10.5, 0, 0),
		})
	)

	planeMonitor.addComponent(
		new OnPointerDown(() => {
			openExternalURL(`https://t.me/${LINK}`)
		})
	)

	engine.addEntity(planeMonitor)
	engine.addEntity(tgStand)
}
