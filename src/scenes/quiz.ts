import resources from 'src/resources'
import utils from '../../node_modules/decentraland-ecs-utils/index'

export function Quiz(): void{
    const quiz = new Entity()
    quiz.addComponent(resources.models.quiz.quizGame)
    quiz.addComponent(
        new Transform({
            position: new Vector3(3, 0, 42),
            scale: new Vector3(150, 150, 150),
            rotation: new Quaternion(0, -1, 0),
        })
    )
    engine.addEntity(quiz)
}