import resources from 'src/resources'
// import utils from '../../node_modules/decentraland-ecs-utils/index'
import * as utils from '@dcl/ecs-scene-utils'

export function Bank(): void{
    const bank = new Entity()
    bank.addComponent(resources.models.bank.bankBuilding)
    bank.addComponent(
        new Transform({
            position: new Vector3(-6.5, 0.2, 14.6),
            scale: new Vector3(30, 30, 30),
            rotation: Quaternion.Euler(0, 180, 0)
        })
    )
    engine.addEntity(bank)

    const D = new Entity()
    D.addComponent(resources.models.bank.D)
    D.addComponent(
        new Transform({
            position: new Vector3(-6.5, 0.2, 14.71),
            scale: new Vector3(30, 30, 30),
            rotation: Quaternion.Euler(0, 0, 0)
        })
    )
    engine.addEntity(D)

    const D1 = new Entity()
    D1.addComponent(resources.models.bank.D)
    D1.addComponent(
        new Transform({
            position: new Vector3(-6.5, 0.2, 14.71),
            scale: new Vector3(30, 30, -30),
            rotation: Quaternion.Euler(0, 0, 0)
        })
    )
    engine.addEntity(D1)

    //Transparent
    const myMaterial = new Material()
    let transparentRed = new Color4(1, 0, 0, 0)
    // let transparentRed = new Color3(0, 0, 0)
    myMaterial.albedoColor = transparentRed


    const platform1pos = new Vector3(-15.25, 0.2, 14.75)
    const box = new Entity()

    box.addComponent(new BoxShape())
    box.getComponent(BoxShape).withCollisions = false

    box.addComponent(new Transform({
        position: new Vector3(-10.25, 1.5, 15),
        scale: new Vector3(3, 2, 1),
        rotation: Quaternion.Euler(0, 90, 0)
    }))
    box.addComponent(myMaterial)
    engine.addEntity(box)

    const platform1 = new Entity()
    platform1.addComponent(resources.models.bank.platform)
    platform1.addComponent(
        new Transform({
            position: platform1pos,
            scale: new Vector3(30, 30, 30),
            rotation: Quaternion.Euler(0, 0, 0)
        })
    )
    engine.addEntity(platform1)

    const path: Vector3[] = []
    path[0] = platform1pos
    path[1] = new Vector3(-15.25, 4.0001, 14.75)
    path[2] = new Vector3(-15.25, 4.0002, 14.75)
    path[3] = new Vector3(-15.25, 4.0003, 14.75)
    path[4] = new Vector3(-15.25, 4.0001, 14.75)
    path[5] = platform1pos

    const elev1 = box.addComponent(
        new OnPointerDown(
            () => {
                platform1.removeComponent(elev1)
                const getBallonPosition = box.getComponent(Transform).position
                platform1.addComponent(
                    new utils.FollowPathComponent(path, 8, () => {
                        log('Fly finished')
                        platform1.addComponent(elev1)
                    })
                )
            },
            { hoverText: 'Go up!' }
        )
    )

    const platform2pos = new Vector3(-6.75, 3.13, 14.75)
    const box2 = new Entity()

    box2.addComponent(new BoxShape())
    box2.getComponent(BoxShape).withCollisions = false

    box2.addComponent(new Transform({
        position: new Vector3(-0.25, 4.53, 14.5),
        scale: new Vector3(3, 2, 1),
        rotation: Quaternion.Euler(0, 90, 0)
    }))
    box2.addComponent(myMaterial)
    engine.addEntity(box2)

    const platform2 = new Entity()
    platform2.addComponent(resources.models.bank.platform)
    platform2.addComponent(
        new Transform({
            position: platform2pos,
            scale: new Vector3(30, 30, 30),
            rotation: Quaternion.Euler(0, 0, 0)
        })
    )
    engine.addEntity(platform2)

    const path2: Vector3[] = []
    path2[0] = platform2pos
    path2[1] = new Vector3(-6.65, 8.1, 14.75)
    path2[2] = new Vector3(-6.65, 8.12, 14.75)
    path2[3] = new Vector3(-6.65, 8.13, 14.75)
    path2[4] = new Vector3(-6.65, 8.1, 14.75)
    path2[5] = platform2pos

    const elev2 = box2.addComponent(
        new OnPointerDown(
            () => {
                platform2.removeComponent(elev2)
                const getBallonPosition2 = box2.getComponent(Transform).position
                platform2.addComponent(
                    new utils.FollowPathComponent(path2, 8, () => {
                        log('Fly finished')
                        platform2.addComponent(elev2)
                    })
                )
            },
            { hoverText: 'Go up!' }
        )
    )
}