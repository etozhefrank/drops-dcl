import resources from 'src/resources'
import utils from '../../node_modules/decentraland-ecs-utils/index'

export function Dance(): void{
    const dance_floor = new Entity()
    dance_floor.addComponent(resources.models.dance.dance_floor)
    dance_floor.addComponent(
        new Transform({
            position: new Vector3(5.5, 0.14, 28.5),
            scale: new Vector3(160, 160, 160),
            rotation: Quaternion.Euler(0, -90, 0)
        })
    )
    engine.addEntity(dance_floor)
}