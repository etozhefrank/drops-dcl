import resources from "src/resources"

export function Main(): void {

    const MainBuilding = new Entity()
    MainBuilding.addComponent(resources.models.main.mainNew)
    const MainBuildingPosition = new Vector3(-8, -0.1, 20)
    MainBuilding.addComponent(
        new Transform({
            position: MainBuildingPosition,
            scale: new Vector3(680, 680, 680),
            rotation: Quaternion.Euler(0, -90, 0)
        })
    )
    engine.addEntity(MainBuilding)

    const chatBox = new Entity()
    chatBox.addComponent(resources.models.chat.chatBox)
    const chatBoxPos = new Vector3(-8, -0.1, 20)
    chatBox.addComponent(
        new Transform({
            position: new Vector3(0, 1, 5),
            scale: new Vector3(20, 20, 20),
            rotation: Quaternion.Euler(0, -45, 0)
        })
    )
    chatBox.addComponent(
        new OnPointerDown(() => {
            openExternalURL(`http://stream.argreality.com/`)
        })
    )
    engine.addEntity(chatBox)

    
}