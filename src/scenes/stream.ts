import resources from 'src/resources'
import utils from '../../node_modules/decentraland-ecs-utils/index'

export function Stream(): void{
    const stream = new Entity()
    stream.addComponent(resources.models.stream.streamTV)
    stream.addComponent(
        new Transform({
            position: new Vector3(8, 0, 10),
            scale: new Vector3(72, 72, 72),
            rotation: Quaternion.Euler(0, -90, 0)
        })
    )
    engine.addEntity(stream)
}