import resources from 'src/resources'

/**TODO: Close button to input text && Focus on text, when player open */
function createInputeText(canvas: UICanvas): UIInputText {
	const textInput = new UIInputText(canvas)
	textInput.width = '80%'
	textInput.height = '50px'
	textInput.vAlign = 'center'
	textInput.hAlign = 'center'
	textInput.hTextAlign = 'center'
	textInput.vTextAlign = 'center'
	textInput.fontSize = 20
	textInput.placeholder = 'Put OpenSea link'
	textInput.positionY = '200px'
	textInput.isPointerBlocker = true
	return textInput
}

function getAddressAndToken(link: string): string {
	const linkSplit = link.split('/')
	const contractAdress = linkSplit[linkSplit.length - 2]
	const tokenId = linkSplit[linkSplit.length - 1]
	return contractAdress + '/' + tokenId
}

/**TODO: Преобразовать ценник к нормальному формату */
async function getDepositPrice(asset: string): Promise<string | null> {
	return await executeTask(async () => {
		try {
			const response = await fetch(`https://api.opensea.io/api/v1/asset/${asset}`)
			const json = await response.json()
			if (!json.orders) {
				return
			}
			const currentPrice = json.orders[0].current_price
			if (currentPrice) {
				return (+(currentPrice / 2) / 1000000000000000000).toString()
			}
		} catch {
			log('failed to reach asset')
		}
	})
}

function createNftFrame(x: number, y: number, z: number, qx: number, qy: number, qz: number) {
	// Создается canvas, который можно наполнять элементами
	const canvas = new UICanvas()

	const nftEntity = new Entity()
	nftEntity.addComponent(
		new Transform({
			position: new Vector3(x, y + 0.75, z + 0.15),
			scale: new Vector3(3, 3, 3),
			rotation: new Quaternion(qx, qy, qz),
		})
	)
	// nftEntity.addComponent(new NFTShape(''))
	engine.addEntity(nftEntity)

	const uiTrigger = new Entity()
	uiTrigger.addComponent(
		new Transform({
			position: new Vector3(x, y, z),
			scale: new Vector3(1.5, 3, 0.3),
			rotation: new Quaternion(qx, qy, qz),
		})
	)

	// !!! dev белый блок !!!
	uiTrigger.addComponent(new BoxShape())
	const myMaterial = new Material()
	let transparentRed = new Color4(1, 0, 0, 0)
	// let transparentRed = new Color3(0, 0, 0)
	myMaterial.albedoColor = transparentRed
	uiTrigger.addComponent(myMaterial)

	uiTrigger.addComponent(
		new OnPointerDown(() => {
			const textInput = createInputeText(canvas)

			// По идеи, visible отключает текст с экрана, а не должен ли удалять (или вообще удалять из канвас)?
			textInput.onTextSubmit = new OnTextSubmit(async (x) => {
				textInput.visible = false
				const avatarNft = getAddressAndToken(x.text)
				const depositPrice = await getDepositPrice(avatarNft)
				const nftShapeComponent = new NFTShape(`ethereum://${avatarNft}`)
				const nftOpenDialog = new OnPointerDown(() => {
					openNFTDialog(`ethereum://${avatarNft}`, `Deposit price: ${depositPrice} ETH`)
				})
				nftEntity.addComponentOrReplace(nftShapeComponent)
				nftEntity.addComponentOrReplace(nftOpenDialog)
			})
		})
	)
	engine.addEntity(uiTrigger)
}

function placeFrame(x: number, y: number, z: number, qx: number, qy: number, qz: number) {
	const frame = new Entity()
	frame.addComponent(resources.models.nftStand.frame)
	frame.addComponent(
		new Transform({
			position: new Vector3(x, y, z),
			scale: new Vector3(150, 150, 150),
			rotation: Quaternion.Euler(qx, qy, qz),
		})
	)
	engine.addEntity(frame)
}

export function NftWall(): void {
	createNftFrame(-16.25, 1.5, 3.3	, 0, 0, 0)
	createNftFrame(-18.5, 1.5, 3.3, 0, 0, 0)
	createNftFrame(-20.75, 1.5, 3.3, 0, 0, 0)
	createNftFrame(-23, 1.5, 3.3, 0, 0, 0)
	createNftFrame(-25.25, 1.5, 3.3, 0, 0, 0)
	createNftFrame(-27.5, 1.5, 3.3, 0, 0, 0)

	createNftFrame(-29, 1.5, 4.5, 0, 1, 0)
	createNftFrame(-29, 1.5, 7.25, 0, 1, 0)
	createNftFrame(-29, 1.5, 10, 0, 1, 0)
	placeFrame(-22, 0.2, 6, 0, 0, 0)
	placeFrame(-22, 0.2, 8.75, 0, 0, 0)
	placeFrame(-22, 0.2, 11.5, 0, 0, 0)
	placeFrame(-17.75, 0.2, 10.7, 0, -90, 0)
	placeFrame(-20, 0.2, 10.7, 0, -90, 0)
	placeFrame(-22.25, 0.2, 10.7, 0, -90, 0)
	placeFrame(-24.5, 0.2, 10.7, 0, -90, 0)
	placeFrame(-26.75, 0.2, 10.7, 0, -90, 0)
	placeFrame(-29, 0.2, 10.7, 0, -90, 0)

}
