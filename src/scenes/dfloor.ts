const colors = ["#1dccc7", "#ffce00", "#9076ff", "#fe3e3e", "#3efe94", "#3d30ec", "#6699cc"]

@Component('tileFlag')
export class TileFlag {
}

@Component('beat')
export class Beat {
    interval: number
    timer: number
    constructor(interval: number = 0.01){
        this.interval = interval
        this.timer = interval
    }
}

const tiles = engine.getComponentGroup(TileFlag)

export class changeColor implements ISystem {
    update(dt: number) {
        let beat = beatKeeper.getComponent(Beat)
        beat.timer -= dt
        if (beat.timer < 0){
            beat.timer = beat.interval
            for (let tile of tiles.entities) {
                const colorNum = Math.floor(Math.random() * colors.length)
                tile.addComponentOrReplace(tileMaterials[colorNum])
            }
        }
    }
}
engine.addSystem(new changeColor)

// Create materials
let tileMaterials : Material[] = []
for (let i = 0; i < colors.length; i ++){
    let material = new Material()
    material.albedoColor = Color3.FromHexString(colors[i])
    tileMaterials.push(material)
}

// Add Tiles
[0, 1, 2].forEach(x => {
    [0, 1, 2, 3].forEach(z => {
        const tile = new Entity()
        tile.addComponent(new PlaneShape())
        tile.addComponent(new Transform({
            position: new Vector3((x * 2) +3.5, .402, (z * 2) + 25.42),
            rotation: Quaternion.Euler(90, 90, 0),
            scale: new Vector3(2, 2, 2)
        }))
        tile.addComponent(new TileFlag())
        const colorNum = Math.floor(Math.random() * colors.length)
        tile.addComponent(tileMaterials[colorNum])
        engine.addEntity(tile)
    })
})


// const audioClip = new AudioClip("sounds/Vexento.mp3")
// audioClip.loop = true
// const audioSource = new AudioSource(audioClip)


// Singleton to keep track of the beat
let beatKeeper = new Entity()
beatKeeper.addComponent(new Beat(0.2))

// audioSource.playing = true