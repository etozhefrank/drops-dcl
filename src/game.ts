import { NPC, NPCDelay } from '@dcl/npc-scene-utils'
import { AliceDialog } from './modules/dialogData'
import { Wheel } from './scenes/Wheel'
import resources from './resources'
import { Balloon } from './scenes/Balloon'
import { NftWall } from './scenes/NftWall'
import { Dance } from './scenes/Dance'
import { changeColor } from './scenes/dfloor'
import { Quiz } from './scenes/quiz'
import { Stream } from './scenes/stream'
import { Main } from './scenes/Main'
import { Bank } from './scenes/bank'
import { NewsMonitor } from './scenes/NewsMonitor'


import { getUserAccount } from "@decentraland/EthereumController";
import { getProvider, Provider } from "@decentraland/web3-provider";
import * as dclTx from "decentraland-transactions";
import * as eth from "eth-connect";

import { getUserData } from "@decentraland/Identity"
import { getCurrentRealm } from "@decentraland/EnvironmentAPI"
import { buy } from './store/blockchain/index'
// Base scene

/**Video stream*/
/**TODO: Make it*/
const myVideoClip = new VideoClip('')

// #2
const myVideoTexture = new VideoTexture(myVideoClip)

// #3
const myMaterial = new Material()
myMaterial.albedoTexture = myVideoTexture
myMaterial.roughness = 1
myMaterial.specularIntensity = 0
myMaterial.metallic = 0

// Stream
const screen = new Entity()
screen.addComponent(new PlaneShape())
screen.addComponent(
	new Transform({
		position: new Vector3(90, 1, 7),
		// scale: new Vector3(10, 10, 10)
	})
)
screen.addComponent(myMaterial)
screen.addComponent(
	new OnPointerDown(() => {
		myVideoTexture.playing = !myVideoTexture.playing
	})
)
engine.addEntity(screen)

// #5
myVideoTexture.play()

/** Main */
Main()

/** NFT Wall */
NftWall()

/**Ballon*/
/**TODO: Change path position*/
Dance()
changeColor
// Quiz()
Stream()
NewsMonitor()
Balloon()
Bank()

/** Wheel */
Wheel()

/**NPC System*/
/**TODO: Change model, animation, sounds*/
export const alice = new NPC(
	{
		position: new Vector3(-22, 1.6, 16.5),
		rotation: Quaternion.Euler(0, 180, 0),
	},
	resources.models.robots.alice,
	() => {
		// animations
		alice.playAnimation('Hello', true, 2)

		let dummyent = new Entity()
		dummyent.addComponent(
			new NPCDelay(2, () => {
				alice.playAnimation('Talk')
			})
		)
		engine.addEntity(dummyent)

		// sound
		alice.addComponentOrReplace(new AudioSource(resources.sounds.alice))
		alice.getComponent(AudioSource).playOnce()

		// dialog UI
		alice.talk(AliceDialog)
	},
	{
		faceUser: true,
		portrait: {
			path: 'images/portraits/alice.png',
			height: 256,
			width: 256,
			section: {
				sourceHeight: 512,
				sourceWidth: 512,
			},
		},
		onWalkAway: () => {
			alice.playAnimation('Goodbye', true, 2)
		},
	}
)

const colors = ['#1dccc7', '#ffce00', '#9076ff', '#fe3e3e', '#3efe94', '#3d30ec', '#6699cc']

/**   Custom components   */

@Component('tileFlag')
export class TileFlag {}

@Component('beat')
export class Beat {
	interval: number
	timer: number
	constructor(interval: number = 0.5) {
		this.interval = interval
		this.timer = interval
	}
}

@Component('wheelSpin')
export class WheelSpin {
	active: boolean = false
	speed: number = 0
	status: boolean = true
	direction: Vector3 = Vector3.Right()
}

const wheels = engine.getComponentGroup(WheelSpin)

export class RotatorSystem implements ISystem {
	update(dt: number) {
		for (const wheel of wheels.entities) {
			const spin = wheel.getComponent(WheelSpin)
			const transform = wheel.getComponent(Transform)
			if (spin.active) {
				transform.rotate(spin.direction, spin.speed *dt)
				spin.speed -= 0.2
				if (spin.speed <= 0) {
					spin.speed = 0
					spin.active = false
				}
			}
		}
	}
}

engine.addSystem(new RotatorSystem())

let wRot = Quaternion.Euler(0, 90, 0)

const wheel_movable = new Entity()
wheel_movable.addComponent(resources.models.wheel.wheel_moveble)
wheel_movable.addComponent(
	new Transform({
		position: new Vector3(-14.5, 2.8, 24.5),
		scale: new Vector3(150, 150, 150),
		rotation: wRot
	})
)
engine.addEntity(wheel_movable)

wheel_movable.addComponent(new WheelSpin())

const garbageCollector = [0]

let counter = 0
wheel_movable.addComponent(
	new OnPointerDown(
		async (e) => {
			counter++
			log(counter)
			if(counter<=1) {
				const spin = wheel_movable.getComponent(WheelSpin)
				if (!spin.active) {
					spin.active = true
				}
				const random = Math.floor(Math.random() * 3)
				spin.speed = 60 + random*60
				let reverseSpeed = spin.speed

				log('WROOOT in Active: ', wRot.eulerAngles)
				log('inActive', spin)

				setTimeout( buyIt, 11500*(random+1))
				function buyIt() {
					if(random == 0) {
						log('0')
						buy('0x98a6ef846bc04ed09d39f9870e2d05514541a76a', '0', '0')

						wheel_movable.addComponentOrReplace(
							new Transform({
								position: new Vector3(-14.5, 2.8, 24.5),
								scale: new Vector3(150, 150, 150),
								rotation: Quaternion.Euler(0, 90, 0)
							})
						)
					}
					if(random == 1) {
						log('1')
						buy('0x98a6ef846bc04ed09d39f9870e2d05514541a76a', '1', '0')

						wheel_movable.addComponentOrReplace(
							new Transform({
								position: new Vector3(-14.5, 2.8, 24.5),
								scale: new Vector3(150, 150, 150),
								rotation: Quaternion.Euler(0, 90, 0)
							})
						)

					}
					if(random == 2) {
						log('2')
						buy('0x98a6ef846bc04ed09d39f9870e2d05514541a76a', '2', '0')

						wheel_movable.addComponentOrReplace(
							new Transform({
								position: new Vector3(-14.5, 2.8, 24.5),
								scale: new Vector3(150, 150, 150),
								rotation: Quaternion.Euler(0, 90, 0)
							})
						)
					}
				}

				setTimeout(() => {
					spin.direction = Vector3.Right()
					counter = 0
					spin.speed = 0
				}, (11500*(random+1)))

				log("speed: ", random)
			}
		},
		{ button: ActionButton.POINTER, hoverText: 'Spin' }
	)
)
