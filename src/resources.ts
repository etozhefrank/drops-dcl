export default {
	sounds: {
		alice: new AudioClip('sounds/alice.mp3'),
		bob: new AudioClip('sounds/bob.mp3'),
		charlie: new AudioClip('sounds/charlie.mp3'),
	},
	models: {
		standard: {
			baseScene: new GLTFShape('models/standard/baseScene.glb'),
		},
		robots: {
			alice: 'models/robots/alice.glb',
			bob: 'models/robots/bob.glb',
			charlie: 'models/robots/charlie.glb',
			rings: new GLTFShape('models/robots/rings.glb'),
		},
		balloon: {
			balloon_moveble: new GLTFShape('models/drops/balloon.glb'),
			balloon_static: new GLTFShape('models/drops/stand_for_balloon.glb'),
		},
		wheel: {
			wheel_moveble: new GLTFShape('models/drops/Wheel_moveble.glb'),
			wheel_static: new GLTFShape('models/drops/Wheel_static.glb'),
		},
		nftStand: {
			stand: new GLTFShape('models/drops/nft_stand.glb'),
			frame: new GLTFShape('models/drops/frame.glb'),
		},
		newsMonitor: {
			telegram_stand: new GLTFShape('models/drops/tg_monitor_stand.glb'),
			telegram_monitor: new GLTFShape('models/drops/tg_monitor.glb'),
		},
		dance: {
			dance_floor: new GLTFShape('models/drops/dance_draft.glb'),
		},
		stream: {
			streamTV: new GLTFShape('models/drops/stream.glb'),
		},
		quiz: {
			quizGame: new GLTFShape('models/drops/quiz.glb'),
		},
		main: {
			// mainBuilding: new GLTFShape('models/drops/mainBackup.glb'),
			mainNew: new GLTFShape('models/drops/mainNew.glb'),
			// mainP1: new GLTFShape('models/drops/bankp1.glb'),
			// mainP2: new GLTFShape('models/drops/bankp2.glb')
		},
		bank: {
			// bankBuilding: new GLTFShape('models/drops/bank.glb'),
			bankBuilding: new GLTFShape('models/drops/bank_new_2.glb'),
			D: new GLTFShape('models/drops/D.glb'),
			platform: new GLTFShape('models/drops/platformNEW.glb')
		},
		chat: {
			chatBox: new GLTFShape('models/drops/CHAT.glb')
		}
	},
	textures: {
		blank: new Texture('images/ui/blank.png'),
		buttonE: new Texture('images/ui/buttonE.png'),
		buttonF: new Texture('images/ui/buttonF.png'),
		leftClickIcon: new Texture('images/ui/leftClickIcon.png'),
		textPanel: new Texture('images/ui/textPanel.png'),
	},

	/** Randomize items */
	spin: {
		hat1: {
			id: 1,
			item: 'some1',
			speed: 18,
		},
		hat2: {
			item: 'some2',
			speed: 36,
		},
		hat3: {
			item: 'some3',
			speed: 54,
		},
		hat4: {
			item: 'some4',
			speed: 72,
		},
		hat5: {
			item: 'some5',
			speed: 90,
		},
		hat6: {
			item: 'some6',
			speed: 108,
		},
		hat7: {
			item: 'some7',
			speed: 126,
		},
		hat8: {
			item: 'some8',
			speed: 144,
		},
		hat9: {
			item: 'some9',
			speed: 162,
		},
		hat10: {
			item: 'some10',
			speed: 180,
		},
		hat11: {
			item: 'some11',
			speed: 198,
		},
		hat12: {
			item: 'some12',
			speed: 216,
		},
		hat13: {
			item: 'some13',
			speed: 234,
		},
		hat14: {
			item: 'some14',
			speed: 252,
		},
		hat15: {
			item: 'some15',
			speed: 270,
		},
		hat16: {
			item: 'some16',
			speed: 288,
		},
		hat17: {
			item: 'some17',
			speed: 306,
		},
		hat18: {
			item: 'some18',
			speed: 324,
		},
		hat19: {
			item: 'some19',
			speed: 342,
		},
		hat20: {
			item: 'some20',
			speed: 360,
		},
	},
}
